'''
Created on Oct 12, 2010
Decision Tree Source Code for Machine Learning in Action Ch. 3
@author: Peter Harrington
'''
from math import log
import operator

def createDataSet():
    dataSet = [[1, 1, 'yes'],
               [1, 1, 'yes'],
               [1, 0, 'no'],
               [0, 1, 'yes'],
               [0, 1, 'no'],
               [0, 1, 'no']]
    labels = ['no surfacing','flippers']
    #change to discrete values
    return dataSet, labels

def calcShannonEnt(dataSet):
    numEntries = len(dataSet)
    labelCounts = {}
    for featVec in dataSet: #the the number of unique elements and their occurance
        currentLabel = featVec[-1]
        if currentLabel not in labelCounts.keys():
            labelCounts[currentLabel] = 0
        labelCounts[currentLabel] += 1
    shannonEnt = 0.0
    for key in labelCounts:
        prob = float(labelCounts[key])/numEntries
        print labelCounts[key], numEntries
        print "calcprob : ", prob
        print "calclog : ", log(prob, 2)
        shannonEnt -= prob * log(prob,2) #log base 2
    print "calcshannonEnt : ", shannonEnt
    return shannonEnt

def calcGiniIndex(dataset):
    numdataset = len(dataset)
    classtag = [line[-1] for line in dataset]
    tagcount = {}
    for value in classtag:
        if value not in tagcount:
            tagcount[value] = 0
        tagcount[value] += 1
    prob = 0.0
    for key in tagcount:
        prob += pow(float(tagcount[key])/numdataset, 2)
    return 1 - prob

def CARTalgorith(tree):
    if tree != None:
       pass
       pass

def splitDataSet(dataSet, axis, value):
    retDataSet = []
    for featVec in dataSet:
        if featVec[axis] == value:
            reducedFeatVec = featVec[:axis]     #chop out axis used for splitting
            reducedFeatVec.extend(featVec[axis+1:])
            retDataSet.append(reducedFeatVec)
    return retDataSet
    
def chooseBestFeatureToSplit(dataSet):
    numFeatures = len(dataSet[0]) - 1      #the last column is used for the labels
    baseEntropy = calcShannonEnt(dataSet)
    baseEntropy = calcGiniIndex(dataSet)
    print("base entropy : %d" %baseEntropy)
    bestInfoGainRate = 0.0; bestFeature = -1;bestInfoGain = 0.0
    for i in range(numFeatures):        #iterate over all the features
        featList = [example[i] for example in dataSet]#create a list of all the examples of this feature
        uniqueVals = set(featList)       #get a set of unique values
        newEntropy = 0.0
        for value in uniqueVals: # split the data set and extract data set by feature value
            subDataSet = splitDataSet(dataSet, i, value)
            print len(subDataSet), len(dataSet)
            prob = float(len(subDataSet))/float(len(dataSet))
            print "value", value
            print "prob ", prob
            print "calcentropy: ", calcShannonEnt(subDataSet)
            #newEntropy += prob * calcShannonEnt(subDataSet)
            newEntropy += prob * calcGiniIndex(subDataSet)
        print "new Entropy", newEntropy, "base Entropy : ", baseEntropy
        infoGain = baseEntropy - newEntropy     #calculate the info gain; ie reduction in entropy
        print("infogain : %d " %infoGain)
        infoGainRate = InfoGainRate(dataSet, uniqueVals, i, infoGain)
        if (infoGainRate > bestInfoGainRate):       #compare this to the best gain so far
            bestInfoGainRate = infoGainRate         #if better than current best, set to best
            bestFeature = i
    return bestFeature                      #returns an integer
# by self
def InfoGainRate(dataset, featset, axis, infoGain):
    datasize= len(dataset)
    featlist = [line[axis] for line in dataset]
    featlistdir = {}
    for value in featlist:
        if value not in featlistdir:
            featlistdir[value] = 0
        featlistdir[value] += 1
    featureentropy = 0.0
    unifeatlist = set(featlist)
    print featlistdir
    print featlist
    print unifeatlist
    print infoGain
    for key in unifeatlist:
        print key
        featureentropy -= (float(featlistdir[key])/datasize) * log(float(featlistdir[key])/datasize, 2)
    return featureentropy

def majorityCnt(classList):
    classCount={}
    for vote in classList:
        if vote not in classCount.keys(): classCount[vote] = 0
        classCount[vote] += 1
    sortedClassCount = sorted(classCount.iteritems(), key=operator.itemgetter(1), reverse=True)
    return sortedClassCount[0][0]

def createTree(dataSet,labels):
    classList = [example[-1] for example in dataSet]
    if classList.count(classList[0]) == len(classList): 
        return classList[0]#stop splitting when all of the classes are equal
    if len(dataSet[0]) == 1: #stop splitting when there are no more features in dataSet
        return majorityCnt(classList)
    print "size : ",len(dataSet)
    bestFeat = chooseBestFeatureToSplit(dataSet)
    bestFeatLabel = labels[bestFeat]
    myTree = {bestFeatLabel:{}}
    del(labels[bestFeat])
    featValues = [example[bestFeat] for example in dataSet]
    uniqueVals = set(featValues)
    for value in uniqueVals:
        subLabels = labels[:]       #copy all of labels, so trees don't mess up existing labels
        myTree[bestFeatLabel][value] = createTree(splitDataSet(dataSet, bestFeat, value),subLabels)
    return myTree                            

def cuttree(tree, dataset):
    pass

def classify(inputTree,featLabels,testVec):
    firstStr = inputTree.keys()[0]
    secondDict = inputTree[firstStr]
    featIndex = featLabels.index(firstStr)
    key = testVec[featIndex]
    valueOfFeat = secondDict[key]
    if isinstance(valueOfFeat, dict): 
        classLabel = classify(valueOfFeat, featLabels, testVec)
    else: classLabel = valueOfFeat
    return classLabel

def storeTree(inputTree,filename):
    import pickle
    fw = open(filename,'w')
    pickle.dump(inputTree,fw)
    fw.close()
    
def grabTree(filename):
    import pickle
    fr = open(filename)
    return pickle.load(fr)
def LoadDatafromFile(filename):
    if filename:
        dataset = []
        label = ["feature1", "feature2","feature3", "feature4"]
        with open(filename) as fn:
          for line in fn.readlines():
            data = line.split()[0:]
            dataset.append(data)
        return dataset ,label
    else:
        print "file name failed"

    