import Ch02.kNN as test_knn
import Ch03.DT_tree as DT_main
import os
import Ch04.bayes_main as bayes
if __name__ == "__main__":
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    #test_knn.printtest()
    #test_knn.datingClassTest()
    bayes.bayes_main()
